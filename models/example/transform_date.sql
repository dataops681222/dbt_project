{{ config(
    materialized='table'
) }}

WITH source_data AS (
    SELECT
        id,
        name,
        category,
        price,
        created_at
    FROM {{ source('bronze', 'products') }}
)

SELECT
    id,
    name,
    category,
    price,
    CAST(created_at AS DATE) AS created_date
FROM source_data
