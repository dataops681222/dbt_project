{{ config(
    materialized='table'
) }}

WITH source_data AS (
    SELECT
        id,
        name,
        category,
        price,
        created_at
    FROM {{ source('bronze', 'products') }}
)

SELECT
    category,
    SUM(price) AS total_price,
    COUNT(id) AS product_count
FROM source_data
GROUP BY category
