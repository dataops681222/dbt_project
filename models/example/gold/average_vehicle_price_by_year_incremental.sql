{{ config(
    materialized='incremental',
    unique_key='vehicle_year'
) }}

WITH source_data AS (
    SELECT
        vehicle_year,
        price,
        _airbyte_emitted_at,
	created_date
    FROM {{ ref('transform_data_incremental') }}
),

max_emitted_at AS (
    SELECT
        MAX(_airbyte_emitted_at) AS max_emitted_at
    FROM {{ source('bronze', 'products') }}
    GROUP BY category
)


SELECT
    vehicle_year,
    AVG(price) AS average_price
FROM source_data
{% if is_incremental() %}
WHERE _airbyte_emitted_at > (SELECT max_emitted_at FROM max_emitted_at)
{% endif %}
GROUP BY vehicle_year