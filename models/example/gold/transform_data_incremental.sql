{{ config(
    materialized='incremental',
    unique_key='id'
) }}

WITH source_data AS (
    SELECT
        id,
        name,
        category,
        price,
        created_at,
        _airbyte_emitted_at
    FROM {{ source('bronze', 'products') }}
),

max_emitted_at AS (
    SELECT
        MAX(_airbyte_emitted_at) AS max_emitted_at
    FROM {{ source('bronze', 'products') }}
    GROUP BY category
)

SELECT
    id,
    name,
    category,
    price,
    CAST(created_at AS DATE) AS created_date,
    _airbyte_emitted_at,
    YEAR(created_date) as vehicle_year
FROM source_data
{% if is_incremental() %}
WHERE _airbyte_emitted_at > (SELECT max_emitted_at FROM max_emitted_at)
{% endif %}