{{ config(
    materialized='table'
) }}

WITH source_data AS (
    SELECT
        created_date,
        price
    FROM {{ ref('transform_date') }}
)

SELECT
    YEAR(created_date) as vehicle_year,
    AVG(price) AS average_price
FROM source_data
GROUP BY vehicle_year

