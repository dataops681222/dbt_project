{{ config(
    materialized='table'
) }}

select 
    user_id,
    address_id
from {{ source('bronze', 'users_address') }}
