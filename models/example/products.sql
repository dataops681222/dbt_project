{{ config(
    materialized='table'
) }}

select 
    id,
    name,
    category,
    price,
    created_at
from {{ source('bronze', 'products') }}

