{% macro generate_schema_name(custom_schema_name, node) -%}
    {%- if target.name == 'gold' -%}
        gold_schema
    {%- else -%}
        {{ custom_schema_name | trim }}
    {%- endif -%}
{%- endmacro %}
